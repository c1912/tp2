#pragma once

#include "Point.h"

class Triangle 
{
private:
    Point a, b, c;

public:
    void SetPointA(Point a);
    void SetPointB(Point b);
    void SetPointC(Point c);
    Point GetPointA();
    Point GetPointB();
    Point GetPointC();

    float GetAB() const;
    float GetAC() const;
    float GetBC() const;

    float Base() const;
    float Longueur() const;
    double Hauteur() const;
    double Surface() const;

    bool Isocele() const;
    bool TriangleRectangle() const;
    bool Equilateral() const;
};