#pragma once

#include "point.h"

class Rectangle 
{
private:
    int longueur;
    int largeur;
    Point pSupGauche;

public:
    void SetLongueur(int longueur);
    void SetLargeur(int largeur);
    void SetPSupGauche(Point pSupGauche);

    int GetLongueur() const;
    int GetLargeur() const;
    Point GetPSupGauche();

    int Perimetre() const;
    int Surface() const;

    bool PerimetrePlusGrand(Rectangle rec) const;
    bool SurfacePlusGrande(Rectangle rec) const;

};
