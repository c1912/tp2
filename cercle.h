#pragma once

#include "point.h"

class Cercle 
{
private:
    Point centre;
    int diametre;

public:
    void SetCentre(Point centre);
    void SetDiametre(int diametre);

    Point GetCentre();
    int GetDiametre() const;

    double Perimetre() const;
    double Surface() const;

    bool SurLeCercle(Point p) const;
    bool DansLeCercle(Point p) const;
    
    bool PerimetrePlusGrand(Cercle c) const;
    bool SurfacePlusGrande(Cercle c) const;

};