#include "rectangle.h"

void Rectangle::SetLongueur(int longueur) 
{
    this->longueur = longueur;
}

void Rectangle::SetLargeur(int largeur) 
{
    this->largeur = largeur;
}

void Rectangle::SetPSupGauche(Point pSupGauche) 
{
    this->pSupGauche = pSupGauche;
}

int Rectangle::GetLongueur() const 
{
    return this->longueur;
}

int Rectangle::GetLargeur() const 
{
    return this->largeur;
}

Point Rectangle::GetPSupGauche() 
{
    return this->pSupGauche;
}

int Rectangle::Perimetre() const 
{
    return ((this->longueur + this->largeur) * 2);
}

int Rectangle::Surface() const 
{
    return (this->longueur * this->largeur);
}

bool Rectangle::PerimetrePlusGrand(Rectangle r) const 
{
    if (this->Perimetre() > r.Perimetre()) 
        return true;
    else 
        return false;
}

bool Rectangle::SurfacePlusGrande(Rectangle r) const 
{
    if (this->Surface() > r.Surface()) 
        return true;
    else 
        return false;
}