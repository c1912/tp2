#include "triangle.h"
#include <math.h>

void Triangle::SetPointA(Point a) 
{
    this->a = a;
}

void Triangle::SetPointB(Point b) 
{
    this->b = b;
}

void Triangle::SetPointC(Point c) 
{
    this->c = c;
}

Point Triangle::GetPointA() 
{
    return this->a;
}

Point Triangle::GetPointB() 
{
    return this->b;
}

Point Triangle::GetPointC() 
{
    return this->c;
}

float Triangle::Base() const 
{
    float base;
    base = GetAB() > GetAC() ? GetAB() : GetAC();
    base = GetBC() > base ? GetBC() : base;
    return base;
}

double Triangle::Surface() const 
{
    double surface;
    surface = ((b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y)) / 2.0;
    return fabs(surface);
}

float Triangle::GetAB() const
{
    float longueur, dX, dY;
    dX = fabsf(a.x - b.x);
    dY = fabsf(a.y - b.y);
    longueur = sqrtf((dX * dX) + (dY * dY));
    return fabsf(longueur);
}

float Triangle::GetAC() const 
{
    float longueur, dX, dY;
    dX = fabsf(a.x - c.x);
    dY = fabsf(a.y - c.y);
    longueur = sqrtf((dX * dX) + (dY * dY));
    return fabsf(longueur);
}

float Triangle::GetBC() const 
{
    float dX = fabsf(b.x - c.x);
    float dY = fabsf(b.y - c.y);
    float longueur = sqrtf((dX * dX) + (dY * dY));
    return fabsf(longueur);
}

double Triangle::Hauteur() const 
{
    double hauteur = ((2 * Surface()) / Base());
    return hauteur;
}

float Triangle::Longueur() const 
{
    return (GetAB() + GetBC() + GetAC());
}

bool Triangle::Isocele() const 
{
    return  (GetAB() == GetAC())
        or (GetAC() == GetBC())
        or (GetBC() == GetAB());
}

bool Triangle::TriangleRectangle() const 
{
    return  (GetAB() * GetAB() == GetAC() * GetAC() + GetBC() * GetBC())
        or (GetAC() * GetAC() == GetAB() * GetAB() + GetBC() * GetBC())
        or (GetBC() * GetBC() == GetAB() * GetAB() + GetAC() * GetAC());
}

bool Triangle::Equilateral() const 
{
    return  (GetAB() == GetAC())
        and (GetAB() == GetBC());
}